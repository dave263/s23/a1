//3

	db.users.insertOne(
			{
				"name": "single",
				"accomodates": 2,
				"price": 1000,
				"description": "A simple room with all the basic necessities",
				"rooms_available": 10,
				"isAvailable": false
			}
		)

//4
db.users.insertMany(
	[
		{
		"name": "double",
		"accomodates": 3,
		"price": 2000,
		"description": "A room fit for a small a family going on a vacation",
		"rooms_availble": 5,
		"isAvailable": false
		},
		{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms_availble": 15,
		"isAvailable": false
		}
	]
)

//5
		db.users.find(
				{
					"name": "double"
				}
			)

//6
		db.users.updateOne(
				{
					"_id" : ObjectId("622019b463c0a7ea38b976a2")
				},
				{
					$set: {
						"rooms_available": 0
					}
				}
			)

//7
	db.users.deleteMany(
			{ "rooms_available": { $eq: 0 } }
		)

